# History configuration
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory

# ZSH Options
setopt autocd
setopt correct

# zsh-autosuggestions configuration
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#00BFFF"
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh

# Set GPG agent for ssh
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent

# Import functions
source ~/.zsh_functions

# Aliases
alias ls=lsd
alias nano=micro
alias icat="kitty +kitten icat"

# Neofetch
neofetch 

# Starship prompt
eval "$(starship init zsh)"

# Dotfile management
alias cfg='git --git-dir=$HOME/Dotfiles --work-tree=$HOME'

# Golang configuration
export GOPATH="$HOME/.go"
export PATH="$GOPATH/bin:$PATH"

# Scripts
alias mdp='~/Scripts/markdown/markdown'

