#!/bin/bash
eval $(xdotool getmouselocation --shell)
if [ $X -le 1920 ]; then
	MON=mon1
else
	MON=mon2
fi
POLYBAR_PID=$(ps aux | grep "polybar -r $MON" | sed -e "s/$USER\s*//" -e "s/\s\s.*//" | head -n 1)
polybar-msg -p $POLYBAR_PID cmd toggle
xte 'keydown Alt_L' 'keydown Shift_L' 'key o' 'keyup Shift_L' 'keyup Alt_L'
